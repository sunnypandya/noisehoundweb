import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SigninComponent } from './signin/signin.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { NavsidebarComponent } from './navsidebar/navsidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavtopComponent } from './navtop/navtop.component';
import { LogoutComponent } from './logout/logout.component';
import { HighchartsChartModule  } from 'highcharts-angular';
import {NgbDatepickerModule, NgbTimepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';



@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    HomeComponent,
    NavsidebarComponent,
    DashboardComponent,
    NavtopComponent,
    LogoutComponent
  ],
  imports: [  
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    HighchartsChartModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
