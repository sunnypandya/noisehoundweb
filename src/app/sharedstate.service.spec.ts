import { TestBed, inject } from '@angular/core/testing';

import { SharedstateService } from './sharedstate.service';

describe('SharedstateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedstateService]
    });
  });

  it('should be created', inject([SharedstateService], (service: SharedstateService) => {
    expect(service).toBeTruthy();
  }));
});
