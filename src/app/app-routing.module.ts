import { NgModule } from '@angular/core';
import {RouterModule, Routes } from '@angular/router';
import {SigninComponent} from './signin/signin.component';
import {HomeComponent} from './home/home.component';
import {LogoutComponent} from './logout/logout.component';


const routes: Routes = [
  { path: 'login', component: SigninComponent },
  { path: 'home', component: HomeComponent},
  { path: 'logout', component: LogoutComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
