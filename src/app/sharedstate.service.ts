import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedstateService {
  selectedSite = new Subject<any>();
  selectedSiteName = "";
  getUpdatedSiteId(): Observable<any>{
    return this.selectedSite.asObservable();
  }
  getSelectedSiteName(){
  	return this.selectedSiteName;
  }
  setSelectedSite(siteId){
    this.selectedSite.next(siteId);
  }
  setSelectedSiteName(siteName){
  	this.selectedSiteName = siteName;
  }
  constructor() { }
}
