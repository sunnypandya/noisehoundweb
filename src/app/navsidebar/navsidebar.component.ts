import { Component, OnInit } from '@angular/core';
import { AsyncService } from '../async.service';
import { SharedstateService } from '../sharedstate.service';

@Component({
  selector: 'app-navsidebar',
  templateUrl: './navsidebar.component.html',
  styleUrls: ['./navsidebar.component.css']
})
export class NavsidebarComponent implements OnInit {
  siteData: any;
  constructor(private _asyncservice:AsyncService, private sharedStateService:SharedstateService) { }

  ngOnInit() {
  	this.getSites();
  }
  selectSite(selectedSite: any, siteName: any):void{
      this.sharedStateService.setSelectedSite(selectedSite);
      this.sharedStateService.setSelectedSiteName(siteName);
      //console.log(siteName);
  }
  getSites(){
    this._asyncservice.asynccallget('sitegroups/').subscribe(res => {
      // this.username = res;
      this.siteData = res;
      console.log(this.siteData);
      this.selectSite(this.siteData.results[0].sites[0].station_id, this.siteData.results[0].sites[0].name);
     });
  }
}
