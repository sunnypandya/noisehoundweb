import { Component, OnInit } from '@angular/core';
import { AsyncService } from '../async.service';


@Component({
  selector: 'app-navtop',
  templateUrl: './navtop.component.html',
  styleUrls: ['./navtop.component.css']
})
export class NavtopComponent implements OnInit {
  username: string;
  constructor(private _asyncservice:AsyncService) { }
  
  ngOnInit() {
  	this.getUser();
  }
  getUser(){
    this._asyncservice.asynccallget('users/').subscribe(res => {
      //this.username = res;
      this.username = res.results[0].username;
      //console.log(this.username);;
     });
  }
}
