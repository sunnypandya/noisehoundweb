import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import * as HC_exporting from 'highcharts/modules/exporting';
import { data } from './data';
HC_exporting(Highcharts);
import {SharedstateService} from '../sharedstate.service';
import {AsyncService} from '../async.service';
import { HttpParams } from '@angular/common/http';
import {NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  startDate: NgbDateStruct;
  endDate: NgbDateStruct;
  startTime: NgbTimeStruct = {hour: 13, minute: 30, second: 0};
  endTime: NgbTimeStruct = {hour: 13, minute: 30, second: 0};
  d: Date = new Date();
  timeNow: Date = new Date();
  tzOffset = this.timeNow.getTimezoneOffset();

  siteNoiseData: any;
  currentSiteName: any;
  currentSiteId: any;
  seconds = true;
  laeqData = [];
  la01Data = [];
  la10Data = [];
  la20Data = [];
  la30Data = [];
  la40Data = [];
  la50Data = [];
  la60Data = [];
  la70Data = [];
  la80Data = [];
  la90Data = [];

  allsitesdata = [];
  Container1 = Highcharts;
  Container2 = Highcharts;
  Container3 = Highcharts;
  Container4 = Highcharts;
  chartOptions: any;
  chartOptions2: any;
  chartOptions3: any;
  chartOptions4: any;

  // old_parseData() {
  //
  //   this.currentSiteName = this.sharedstateService.getSelectedSiteName();
  //   // console.log(this.currentSiteName);
  //   this.laeqData = [];
  //   this.la01Data = [];
  //   this.la10Data = [];
  //   this.la20Data = [];
  //   this.la30Data = [];
  //   this.la40Data = [];
  //   this.la50Data = [];
  //   this.la60Data = [];
  //   this.la70Data = [];
  //   this.la80Data = [];
  //   this.la90Data = [];
  //
  //   // console.log(this.siteNoiseData);
  //   for (const result of this.siteNoiseData.data) {
  //       // console.log(result);
  //       this.laeqData.push([result.timestamp * 1000, result.leq_a]);
  //       this.la01Data.push([result.timestamp * 1000, result.l01_a]);
  //       this.la10Data.push([result.timestamp * 1000, result.l10_a]);
  //       this.la20Data.push([result.timestamp * 1000, result.l20_a]);
  //       this.la30Data.push([result.timestamp * 1000, result.l30_a]);
  //       this.la40Data.push([result.timestamp * 1000, result.l40_a]);
  //       this.la50Data.push([result.timestamp * 1000, result.l50_a]);
  //       this.la60Data.push([result.timestamp * 1000, result.l60_a]);
  //       this.la70Data.push([result.timestamp * 1000, result.l70_a]);
  //       this.la80Data.push([result.timestamp * 1000, result.l80_a]);
  //       this.la90Data.push([result.timestamp * 1000, result.l90_a]);
  //   }
  //   this.plotlaeq();
  //   this.plotla90();
  //   this.plotla01();
  //   this.allsitedata();
  // }

  parseData() {

    this.currentSiteName = this.sharedstateService.getSelectedSiteName();
    // console.log(this.currentSiteName);
    this.laeqData = [];
    this.la01Data = [];
    this.la10Data = [];
    this.la20Data = [];
    this.la30Data = [];
    this.la40Data = [];
    this.la50Data = [];
    this.la60Data = [];
    this.la70Data = [];
    this.la80Data = [];
    this.la90Data = [];

    // console.log(this.siteNoiseData);
    for (const result of this.siteNoiseData.data) {
      // console.log(result);
      this.laeqData.push([result.TimeStamp * 1000, result.LEQ_A]);
      this.la01Data.push([result.TimeStamp * 1000, result.L01_A]);
      this.la10Data.push([result.TimeStamp * 1000, result.L10_A]);
      this.la20Data.push([result.TimeStamp * 1000, result.L20_A]);
      this.la30Data.push([result.TimeStamp * 1000, result.L30_A]);
      this.la40Data.push([result.TimeStamp * 1000, result.L40_A]);
      this.la50Data.push([result.TimeStamp * 1000, result.L50_A]);
      this.la60Data.push([result.TimeStamp * 1000, result.L60_A]);
      this.la70Data.push([result.TimeStamp * 1000, result.L70_A]);
      this.la80Data.push([result.TimeStamp * 1000, result.L80_A]);
      this.la90Data.push([result.TimeStamp * 1000, result.L90_A]);
    }
    this.plotlaeq();
    this.plotla90();
    this.plotla01();
    this.allsitedata();
  }

  plotla90() {
    this.chartOptions3 = {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Site Name: ' + this.currentSiteName + ' LA90 (dBA)'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
              dateTimeLabelFormats: {
                    second: '%Y-%m-%d<br/>%H:%M:%S',
                    minute: '%Y-%m-%d<br/>%H:%M',
                    hour: '%Y-%m-%d<br/>%H:%M',
                    day: '%Y<br/>%m-%d',
                    week: '%Y<br/>%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                }
            },
            yAxis: {
                title: {
                    text: 'Site Name: ' + this.currentSiteName + ' (LA90 dBA)'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                },
              line:{
                marker:{
                  enabled: false
                }
              }
            },
            series: [{
                type: 'line',
                name: 'LA90 vs time',
                data: this.la90Data,
                color: '#0066FF'
            }]
        };
  }

  plotla01() {
        this.chartOptions4 =  {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Site Name:' + this.currentSiteName + ' (LA01 dBA)'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
              dateTimeLabelFormats: {
                    second: '%Y-%m-%d<br/>%H:%M:%S',
                    minute: '%Y-%m-%d<br/>%H:%M',
                    hour: '%Y-%m-%d<br/>%H:%M',
                    day: '%Y<br/>%m-%d',
                    week: '%Y<br/>%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                }
            },
            yAxis: {
                title: {
                    text: 'LA01'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                },
              line:{
                marker:{
                  enabled: false
                }
              }
            },
            series: [{
                type: 'line',
                name: 'LA01 vs time',
                data: this.la01Data,
                color: '#000000'
            }]
        };
  }


  allsitedata() {
    this.allsitesdata = [];
    this.allsitesdata.push({name: 'Laeq', data: this.laeqData});
    this.allsitesdata.push({name: 'LA01', data: this.la01Data});
    this.allsitesdata.push({name: 'LA10', data: this.la10Data});
    this.allsitesdata.push({name: 'LA20', data: this.la20Data});
    this.allsitesdata.push({name: 'LA30', data: this.la30Data});
    this.allsitesdata.push({name: 'LA40', data: this.la40Data});
    this.allsitesdata.push({name: 'LA50', data: this.la50Data});
    this.allsitesdata.push({name: 'LA60', data: this.la60Data});
    this.allsitesdata.push({name: 'LA70', data: this.la70Data});
    this.allsitesdata.push({name: 'LA80', data: this.la80Data});
    this.allsitesdata.push({name: 'LA90', data: this.la90Data});
    // console.log(this.allsitesdata);
    this.chartOptions2 = {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Site Name: '+this.currentSiteName+ '(All stats dBA)'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
              dateTimeLabelFormats: {
                    second: '%Y-%m-%d<br/>%H:%M:%S',
                    minute: '%Y-%m-%d<br/>%H:%M',
                    hour: '%Y-%m-%d<br/>%H:%M',
                    day: '%Y<br/>%m-%d',
                    week: '%Y<br/>%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                }
            },
            yAxis: {
                title: {
                    text: 'All stats dBA'
                }
            },
            legend: {
                enabled: true
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                },
              line:{
                marker:{
                  enabled: false
                }
              }
            },
            series: this.allsitesdata
        };
  }

  plotlaeq() {
    this.chartOptions = {
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Site Name: ' + this.currentSiteName + ' (LAeq(dBA))'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
                 // labels: {format:'{value:%Y-%b-%e %H %M %S}'}
                 dateTimeLabelFormats: {
                    second: '%Y-%m-%d<br/>%H:%M:%S',
                    minute: '%Y-%m-%d<br/>%H:%M',
                    hour: '%Y-%m-%d<br/>%H:%M',
                    day: '%Y<br/>%m-%d',
                    week: '%Y<br/>%m-%d',
                    month: '%Y-%m',
                    year: '%Y'
                }
            },
            yAxis: {
                title: {
                    text: 'LAeq'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                },
              line:{
                marker:{
                  enabled: false
                }
              }
            },
            series: [{
                type: 'line',
                name: 'LAeq vs time',
                data: this.laeqData
            }]
        };
  }

  getSiteData(siteId: any) {
    let params = new HttpParams();
    params = params.append('site_id', siteId);
    this._asyncservice.asynccallgetqparam('noise_data/', params).subscribe(res => {
      this.siteNoiseData = res;
      this.parseData();
     });
  }

  getEpoch(localDate, localTime) {
    this.d.setDate(localDate.day);
    this.d.setFullYear(localDate.year);
    this.d.setMonth(parseInt(localDate.month) - 1);
    this.d.setHours(localTime.hour);
    this.d.setMinutes(localTime.minute);
    this.d.setSeconds(localTime.second);
    // console.log(this.d);
    console.log(Math.floor((this.d.getTime() / 1000)) - (this.tzOffset * 60));
    return Math.floor(this.d.getTime() / 1000 - (this.tzOffset * 60));
  }

  getFilteredSiteData() {
    let params = new HttpParams();
    params = params.append('site_id', this.currentSiteId);
    params = params.append('start_time', String(this.getEpoch(this.startDate,this.startTime)));
    params = params.append('end_time', String(this.getEpoch(this.endDate,this.endTime)));
    this._asyncservice.asynccallgetqparam('noise_data/', params).subscribe(res => {
      this.siteNoiseData = res;
      this.parseData();
     });
  }

  constructor(private sharedstateService: SharedstateService, private _asyncservice: AsyncService) { }

  ngOnInit() {
    this.sharedstateService.getUpdatedSiteId().subscribe(res => {this.getSiteData(res);this.currentSiteId = res});
  }
}
