import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class AsyncService {
  httpOptions = {
  		headers: new HttpHeaders({ 
  			'Content-Type': 'application/json',
  			'Authorization': localStorage.getItem('Authorization')
  		 })
	};	
  private baseUrl = 'http://103.112.218.249:8888/api/';

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
  private handleError<T> (operation = 'operation', result?: T) {
	  return (error: any): Observable<T> => {
	 
	    // TODO: send the error to remote logging infrastructure
	    console.error(error); // log to console instead
	 
	    // TODO: better job of transforming error for user consumption
	    
	 
	    // Let the app keep running by returning an empty result.
	    return of(result as T);
	  };
  }
  asynccallget(url: string):Observable<any>{
		return this.http.get<any>(this.baseUrl+url,this.httpOptions).pipe(
    		catchError(this.handleError<any>('Error'))
  		);			
  }
  asynccallgetqparam(url: string, dataparam:any):Observable<any>{
		return this.http.get<any>(this.baseUrl+url,{headers: this.httpOptions.headers, params: dataparam}).pipe(
    		catchError(this.handleError<any>('Error'))
  		);			
  }
  constructor(private http: HttpClient) { }
}
