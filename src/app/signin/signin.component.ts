import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  username: string;
  password: string;
  token: any;
  constructor(private authService: AuthService,private router: Router) { }

  checkForLogin(){
  	if(localStorage.getItem('Authorization')){
  		this.router.navigate(['/home']);
  	}
  }
  
  ngOnInit() {
  	this.checkForLogin();
  }
  
  onSubmit(){
  	this.authService.getUserToken(this.username,this.password).subscribe(token => {
  	 	this.token = token;
  	 	if(this.token.token){
  	 		localStorage.setItem('Authorization','Token '+this.token.token);
  	 		this.router.navigate(['/home']);
  	 	}else{
  	 		console.log('Incorrect Credentials');
  	 	}
  	 });
  }
}
